package mod.steamnsteel.client.collada.xmltransformer.collada;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.IColladaInputStream;
import mod.steamnsteel.client.collada.model.ColladaModel;
import mod.steamnsteel.client.collada.model.ColladaNode;
import mod.steamnsteel.client.collada.model.ColladaSampler;
import mod.steamnsteel.client.collada.model.ColladaSamplerEntry;
import mod.steamnsteel.client.collada.model.transformation.TransformationBase;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.xml.xpath.XPathExpressionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Steven on 7/05/2015.
 */
public class SamplerTransformer extends ColladaTransformerBase<ColladaSampler> {
    private final String target;
    private final Node root;
    private final ColladaModel model;

    public SamplerTransformer(String target, HashMap<String, Node> identifiedNodes, Node root, ColladaModel model) {
        super(identifiedNodes);
        this.target = target;

        this.root = root;
        this.model = model;
    }

    @Nullable
    @Override
    public ColladaSampler apply(@Nullable Node node) {
        try {
            ColladaSampler sampler = new ColladaSampler(target);

            LinkedList<IColladaInputStream> inputs = getInputs(node, root, false);
            Iterable<ColladaNode> allNodes = model.getAllNodes();
            Iterable<ColladaNode> selectableNodes = Iterables.filter(allNodes, new Predicate<ColladaNode>() {
                public boolean apply(@Nullable ColladaNode colladaNode) {
                    return colladaNode.getSid() != null && !"".equals(colladaNode.getSid());
                }
            });


            ImmutableMap<String, ColladaNode> map = Maps.uniqueIndex(selectableNodes, new Function<ColladaNode, String>() {
                @Nullable
                @Override
                public String apply(@Nullable ColladaNode colladaNode) {
                    return colladaNode.getSid();
                }
            });

            if (!map.containsKey(sampler.targetElement)) {
                //This should never hit, why are you animating an element I can't find?
                return sampler;
            }

            ColladaNode node1 = map.get(sampler.targetElement);

            Float defaultValue = null;

            for (TransformationBase transformationBase : node1.getTransformations()) {
                if (transformationBase.getSid().equals(sampler.targetTransform)) {
                    defaultValue = (Float)transformationBase.getValue(sampler);
                    break;
                }
            }

            int index = 0;

            ArrayList<ColladaSamplerEntry> entries = new ArrayList<>();
            boolean wasValid = true;
            while (wasValid) {
                ColladaSamplerEntry entry = new ColladaSamplerEntry();
                for (IColladaInputStream input : inputs) {
                    wasValid &= input.applyElementsAtIndex(index, entry);
                }
                index++;
                if (wasValid) {
                    entries.add(entry);
                }
            }

            //TODO: Optimise samples Ideally I'd like to optimise them away entirely, but I'm not sure how to do it.
            //      The populators only support writing to elements and I don't even have access to them from here.
            sampler.addEntry(entries.get(0));

            for (int i = 1; i < entries.size() - 1; ++i) {
                ColladaSamplerEntry thisEntry = entries.get(i);
                float previousValue = entries.get(i - 1).value;
                float nextValue = entries.get(i - 1).value;
                if (thisEntry.value != previousValue || thisEntry.value != nextValue) {
                    sampler.addEntry(thisEntry);
                }
            }

            LinkedList<ColladaSamplerEntry> samplerEntries = sampler.samplerEntries;
            if (samplerEntries.size() > 1) {
                sampler.addEntry(entries.get(entries.size()-1));
            }

            if (defaultValue != null && samplerEntries.size() > 0) {
                if (samplerEntries.size() == 2 && samplerEntries.get(0).value == defaultValue && samplerEntries.get(1).value == defaultValue) {
                    return null;
                } else if (samplerEntries.size() == 1 && samplerEntries.get(0).value == defaultValue) {
                    return null;
                }
            }

            return sampler;
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            throw new ColladaException("Unable to create Sampler", e);
        }
    }
}
