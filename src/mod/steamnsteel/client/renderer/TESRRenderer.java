package mod.steamnsteel.client.renderer;

import mod.steamnsteel.client.collada.model.*;
import mod.steamnsteel.client.collada.model.transformation.MatrixTransform;
import net.minecraftforge.client.model.obj.Vertex;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Matrix4f;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.*;

/**
 * Created by Steven on 29/05/2015.
 */
public class TESRRenderer {
    Map<UUID, List<ModelInstance>> instances = new HashMap<>();
    Map<UUID, ColladaModel> models = new HashMap<>();
    Map<Integer, Map<Integer, List<UUID>>> renderPassMaterials = new HashMap<>();
    private Integer maximumItemsPerBatch;

    public void queueRender(ModelInstance instance) {
        ColladaModel model = instance.getModel();
        UUID id = model.getId();
        List<ModelInstance> modelInstances = this.instances.get(id);
        if (modelInstances == null) {
            modelInstances = new LinkedList<>();
            instances.put(id, modelInstances);

            analyseMaterials(model);
        }
        modelInstances.add(instance);
        //TODO: Is this a good place to Update animations?

        instance.updateAnimation();
    }

    private void analyseMaterials(ColladaModel model) {
        if (models.containsKey(model.getId())) {
            return;
        }
        models.put(model.getId(), model);
        for (ColladaMaterial colladaMaterial : model.getMaterials()) {

            int renderPass = colladaMaterial.getRenderPass();
            Map<Integer, List<UUID>> materialMap = renderPassMaterials.get(renderPass);
            if (materialMap == null) {
                materialMap = new HashMap<>();
                renderPassMaterials.put(renderPass, materialMap);
            }

            int textureId = colladaMaterial.getTextureId();
            List<UUID> models = materialMap.get(textureId);
            if (models == null) {
                models = new LinkedList<>();
                materialMap.put(textureId, models);
            }

            if (!models.contains(model)) {
                models.add(model.getId());
            }
        }
    }

    public void render() {
        if (maximumItemsPerBatch == null) {
            //FIXME: Determine what maximumItemsPerBatch should be
            maximumItemsPerBatch = 100;
        }

        for (int i = 0; i < 1; i++) {
            renderPass(i);
        }
        for (Map.Entry<UUID, List<ModelInstance>> modelInstances : instances.entrySet()) {
            modelInstances.getValue().clear();
        }
    }

    private void renderPass(int pass) {
        Map<Integer, List<UUID>> textureModelsMap = renderPassMaterials.get(pass);
        for (Map.Entry<Integer, List<UUID>> textureModels : textureModelsMap.entrySet()) {
            Integer textureMapId = textureModels.getKey();
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureMapId);

            for (UUID modelId : textureModels.getValue()) {
                renderInstancesForTextureId(textureMapId, modelId);
            }
        }
    }

    private void renderInstancesForTextureId(int textureId, UUID modelId) {
        List<ModelInstance> instances = this.instances.get(modelId);
        ColladaModel model = models.get(modelId);
        List<ColladaMeshElementSimple> meshes = model.getMeshesByTextureId(textureId);

        for (ColladaMeshElementSimple mesh : meshes) {
            //Bind mesh for instancing
            int instancesBatched = 0;
            for (ModelInstance instance : instances) {
                MatrixTransform matrix = instance.getTransformForMesh(mesh.getParentNode().getIndex());

                fallbackRenderMesh(mesh, matrix);

                //Assign Matrix to geometry instance in GPU.
                if (++instancesBatched == maximumItemsPerBatch) {
                    //Send to GPU
                    instancesBatched = 0;
                }
            }

            if (instancesBatched > 0) {
                //Send to GPU.
            }
        }
    }

    private static final Vertex WORKING_VERTEX = new Vertex(0, 0, 0);
    private void fallbackRenderMesh(ColladaMeshElementSimple colladaMeshElement, MatrixTransform matrix) {
        GL11.glBegin(colladaMeshElement.getType().drawMode);
        for (ColladaVertex vertex : colladaMeshElement.getVertices()) {
            WORKING_VERTEX.x = vertex.position.x;
            WORKING_VERTEX.y = vertex.position.y;
            WORKING_VERTEX.z = vertex.position.z;

            matrix.apply(WORKING_VERTEX);

            GL11.glNormal3f(vertex.normal.x, vertex.normal.y, vertex.normal.z);
            GL11.glTexCoord2f(vertex.texture.x, vertex.texture.y);
            GL11.glVertex3f(WORKING_VERTEX.x, WORKING_VERTEX.y, WORKING_VERTEX.z);
        }

        GL11.glEnd();
        GL11.glPopMatrix();
    }
}
