package mod.steamnsteel.client.collada.model;

import java.io.Serializable;

/**
 * Created by Steven on 5/05/2015.
 */
public class ColladaMaterial implements Serializable {
    private String diffuseTexture;
    private int textureId;
    private int renderPass = 0;

    public void setDiffuseTexture(String diffuseTexture) {
        this.diffuseTexture = diffuseTexture;
    }

    public String getDiffuseTexture() {
        return diffuseTexture;
    }

    public void setTextureId(int textureId) {
        this.textureId = textureId;
    }

    public int getTextureId() {
        return textureId;
    }

    public int getRenderPass() {
        return renderPass;
    }

    public void setRenderPass(int renderPass) {
        this.renderPass = renderPass;
    }
}
