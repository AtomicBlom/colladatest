package mod.steamnsteel.client.collada.xmltransformer.collada;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import mod.steamnsteel.client.collada.model.ColladaNode;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.model.ColladaMeshElement;
import mod.steamnsteel.client.collada.model.ColladaMeshGeometry;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;
import java.util.HashMap;

/**
 * Created by Steven on 5/05/2015.
 */
public class GeometryTransformer extends ColladaTransformerBase<ColladaMeshGeometry> {

    private final Function<Node, ColladaMeshElement> transformMesh;

    public GeometryTransformer(HashMap<String, Node> identifiedNodes, ColladaNode parentNode) {

        super(identifiedNodes);
        this.transformMesh = new MeshTransformer(identifiedNodes, parentNode);
    }

    @Override
    public ColladaMeshGeometry apply(Node xmlGeometryInstance) {
        try {
            String geometryName = getAttributeSafe(xmlGeometryInstance, "url");

            NodeList meshNode = findNodes(identifiedNodes.get(geometryName.substring(1)), "./mesh");

            ColladaMeshGeometry geometry = new ColladaMeshGeometry();
            geometry.addMeshElements(
                    Iterables.transform(new IterableNodeList(meshNode), transformMesh)
            );
            return geometry;

        } catch (XPathExpressionException e) {
            throw new ColladaException("Error creating Geometry", e);
        }
    }
};