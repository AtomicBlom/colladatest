package mod.steamnsteel.client.collada.xmltransformer.animationdefinition;

import com.google.common.collect.Iterables;
import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.model.ColladaAnimation;
import mod.steamnsteel.client.collada.model.ColladaModel;
import mod.steamnsteel.client.collada.xmltransformer.AnimationDefinitionException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Nullable;
import javax.xml.xpath.XPathExpressionException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steven on 25/05/2015.
 */
public class AnimationDefinitionTransformer extends AnimationDefinitionTransformerBase<ColladaModel> {
    private final ColladaModel model;

    public AnimationDefinitionTransformer(ColladaModel model) {
        this.model = model;
    }

    @Nullable
    @Override
    public ColladaModel apply(@Nullable Node node) {
        try {
            final Node root = findNode(node, "/animations");

            TimeScale timeScale = TimeScale.fromString(getAttributeSafe(root, "timeScale"));
            if (timeScale == null) {
                throw new AnimationDefinitionException("timeScale is a required element");
            }

            String framesPerSecond = getAttributeSafe(root, "framesPerSecond");
            Float fps = null;
            if (framesPerSecond != null) {
                fps = Float.parseFloat(framesPerSecond);
            } else {
                fps = model.getFps();
            }
            if (fps == null && timeScale == TimeScale.FRAMES) {
                throw new AnimationDefinitionException("Could not determine Frames Per Second of animation, specify framesPerSecond on <animation>");
            }

            final NodeList animations = findNodes(root, "./animation");

            List<ColladaAnimation> modelAnimations = model.getAnimations();
            ArrayList<ColladaAnimation> existingAnimations = new ArrayList<>(modelAnimations);
            AnimationTransformer transformer = new AnimationTransformer(existingAnimations, timeScale, fps);

            //Remove all the existing animations.
            modelAnimations.clear();
            Iterables.addAll(
                    modelAnimations,
                    Iterables.transform(new IterableNodeList(animations), transformer)
                    );

            return model;
        } catch (XPathExpressionException e) {
            throw new AnimationDefinitionException("Could not determine root node of animation definition");
        }
    }
}
