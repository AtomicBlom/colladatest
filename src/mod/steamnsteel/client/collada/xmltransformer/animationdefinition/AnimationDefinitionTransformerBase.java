package mod.steamnsteel.client.collada.xmltransformer.animationdefinition;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import mod.steamnsteel.client.collada.ColladaArray;
import mod.steamnsteel.client.collada.IColladaInputStream;
import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import mod.steamnsteel.client.collada.xmltransformer.collada.InputStreamTransformer;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Steven on 7/05/2015.
 */
public abstract class AnimationDefinitionTransformerBase<T> extends TransformerBase<T> {

}
