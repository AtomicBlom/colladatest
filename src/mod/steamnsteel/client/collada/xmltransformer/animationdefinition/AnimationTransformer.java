package mod.steamnsteel.client.collada.xmltransformer.animationdefinition;

import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.model.AnimationType;
import mod.steamnsteel.client.collada.model.ColladaAnimation;
import mod.steamnsteel.client.collada.model.ColladaSampler;
import mod.steamnsteel.client.collada.model.ColladaSamplerEntry;
import mod.steamnsteel.client.collada.xmltransformer.AnimationDefinitionException;
import mod.steamnsteel.utility.log.Logger;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steven on 25/05/2015.
 */
public class AnimationTransformer extends AnimationDefinitionTransformerBase<ColladaAnimation> {

    private final ArrayList<ColladaAnimation> sourceAnimations;
    private final TimeScale timeScale;
    private final Float fps;

    public AnimationTransformer(ArrayList<ColladaAnimation> sourceAnimations, TimeScale timeScale, Float fps) {
        this.sourceAnimations = sourceAnimations;
        this.timeScale = timeScale;
        this.fps = fps;
    }

    @Nullable
    @Override
    public ColladaAnimation apply(@Nullable Node node) {

        String name = getAttributeSafe(node, "name");

        float start = Float.parseFloat(getAttributeSafe(node, "start", "0"));
        if (timeScale == TimeScale.FRAMES) {
            start /= fps;
        }

        ColladaAnimation parentAnimation = new ColladaAnimation(name, AnimationType.ONCE);
        float childStart = start;
        int animationNumber = 0;
        for (Node animationSlice : new IterableNodeList(node.getChildNodes())) {
            String nodeName = animationSlice.getNodeName();
            if ("loop".equals(nodeName) || "once".equals(nodeName)) {
                String sourceAnimations = getAttributeSafe(animationSlice, "source", "*");
                float end = Float.parseFloat(getAttributeSafe(animationSlice, "end", "0"));
                if (timeScale == TimeScale.FRAMES) {
                    end /= fps;
                }

                ColladaAnimation childAnimation = new ColladaAnimation(name + "-" + ++animationNumber, "loop".equals(nodeName) ? AnimationType.LOOP : AnimationType.ONCE);
                Logger.info("slicing " + childAnimation.getName());
                List<ColladaSampler> colladaSamplers = sliceSamplers(sourceAnimations, childStart, end);
                childAnimation.samplers.addAll(colladaSamplers);
                parentAnimation.getChildAnimations().add(childAnimation);
                childStart += end;
            } else if ("choice".equals(nodeName)) {
                throw new AnimationDefinitionException("<choice> is not yet implemented");
            }
        }

        return parentAnimation;
    }

    private List<ColladaSampler> sliceSamplers(String sourceAnimations, float start, float end) {
        ColladaSamplerEntry value;
        List<ColladaSampler> newSamplers = new ArrayList<>();
        int slicedEntries = 0;
        for (ColladaAnimation sourceAnimation : this.sourceAnimations) {
            String name = sourceAnimation.getName();
            if ("*".equals(sourceAnimations) || (name != null && name.equals(sourceAnimations))) {
                for (ColladaSampler sampler : sourceAnimation.samplers) {
                    ++slicedEntries;
                    ColladaSampler newSampler = new ColladaSampler(sampler.targetSID + '.' + sampler.targetField);
                    Logger.info("  Slicing # " + slicedEntries + ": " + newSampler.targetSID + '.' + sampler.targetField);
                    value = sampler.getEntryAtTime(start);
                    if (value != null) {
                        value.timeStamp -= start;
                        newSampler.addEntry(value);
                    }

                    for (ColladaSamplerEntry entry : sampler.samplerEntries) {
                        if (entry.timeStamp <= start) { continue; }
                        if (entry.timeStamp >= end) { break; }

                        value = entry.duplicate();
                        value.timeStamp -= start;
                        newSampler.addEntry(value);
                    }

                    value = sampler.getEntryAtTime(end);
                    if (value != null) {
                        value.timeStamp -= start;
                        newSampler.addEntry(value);
                    }

                    //If entry is valid
                    newSamplers.add(newSampler);
                }
            }
        }
        return newSamplers;
    }
}
