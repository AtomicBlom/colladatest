package mod.steamnsteel.client.collada.xmltransformer.collada;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import mod.steamnsteel.client.collada.*;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Steven on 5/05/2015.
 */
public class InputStreamTransformer extends ColladaTransformerBase<IColladaInputStream> {

    private final Node scopeRoot;
    private final boolean ignoreElementsWithNoName;

    public InputStreamTransformer(Node scopeRoot, HashMap<String, Node> identifiedNodes, boolean ignoreElementsWithNoName) {
        super(identifiedNodes);
        this.scopeRoot = scopeRoot;
        this.ignoreElementsWithNoName = ignoreElementsWithNoName;
    }

    @Nullable
    @Override
    public IColladaInputStream apply(Node input) {
        try {
            String semantic = getAttributeSafe(input, "semantic");
            int offset = Integer.parseInt(getAttributeSafe(input, "offset", "0"));
            String source = getAttributeSafe(input, "source");

            Node sourceNode = identifiedNodes.get(source.substring(1));

            IColladaInputStream inputStream;
            if (!"vertices".equals(sourceNode.getNodeName())) {
                AccessorTransformer accessorTransformer = new AccessorTransformer(scopeRoot, identifiedNodes, ignoreElementsWithNoName);
                ColladaAccessor accessor = accessorTransformer.apply(sourceNode);

                inputStream = new ColladaInputStream(accessor, semantic, offset);

            } else {
                NodeList inputNodeList = findNodes(sourceNode, "./input");

                LinkedList<IColladaInputStream> childStreams = Lists.newLinkedList(
                        Iterables.transform(new IterableNodeList(inputNodeList), this)
                );

                inputStream = new ColladaVertexInputStream(childStreams, semantic, offset);
            }
            return inputStream;

        } catch (Exception e) {
            throw new ColladaException("Error creating InputStream", e);
        }
    }
}