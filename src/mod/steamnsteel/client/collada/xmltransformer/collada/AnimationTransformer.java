package mod.steamnsteel.client.collada.xmltransformer.collada;

import com.google.common.collect.Iterables;
import mod.steamnsteel.client.collada.model.AnimationType;
import mod.steamnsteel.client.collada.model.ColladaAnimation;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.model.ColladaModel;
import mod.steamnsteel.client.collada.model.ColladaSampler;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Nullable;
import javax.xml.xpath.XPathExpressionException;
import java.util.HashMap;

/**
 * Created by Steven on 7/05/2015.
 */
public class AnimationTransformer extends ColladaTransformerBase<ColladaAnimation> {
    private final ColladaModel model;

    public AnimationTransformer(HashMap<String, Node> identifiedElements, ColladaModel model) {
        super(identifiedElements);
        this.model = model;
    }

    @Nullable
    @Override
    public ColladaAnimation apply(@Nullable Node node) {
        try {
            ColladaAnimation animation = new ColladaAnimation(getAttributeSafe(node, "name"), AnimationType.LOOP);
            NodeList childAnimationNodes = findNodes(node, "./animation");
            animation.addChildAnimations(Iterables.transform(new IterableNodeList(childAnimationNodes), this));

            Node channelNode = findNode(node, "./channel");
            if (channelNode == null) {
                //not sure what to do here to be honest.
                return null;
            }
            String target = getAttributeSafe(channelNode, "target");

            SamplerTransformer transformer = new SamplerTransformer(target, identifiedNodes, node, model);

            Node samplerNode = identifiedNodes.get(getAttributeSafe(channelNode, "source").substring(1));
            ColladaSampler sampler = transformer.apply(samplerNode);
            if (sampler == null) {
                return null;
                //animation.addSampler(sampler);
            }
            animation.addSampler(sampler);
            return animation;
        } catch (XPathExpressionException e) {
            throw new ColladaException("Could not create Animation", e);
        }
    }
}
