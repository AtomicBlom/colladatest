package mod.steamnsteel.client.collada.model;

import com.google.common.base.Objects;
import mod.steamnsteel.utility.math.SNSMaths;

import java.io.Serializable;
import java.util.LinkedList;

public class ColladaSampler implements Serializable {
    public final String targetSID;
    public final String targetField;
    public final String targetTransform;
    public final String targetElement;
    public final LinkedList<ColladaSamplerEntry> samplerEntries = new LinkedList<>();


    public float getMaxTime() {
        return samplerEntries.getLast().timeStamp;
    }

    public ColladaSampler(String target) {
        int dotLocation = target.lastIndexOf('.');
        int slashLocation = target.indexOf('/');
        this.targetSID = target.substring(0, dotLocation);
        this.targetElement = target.substring(0, slashLocation);
        this.targetTransform = target.substring(slashLocation + 1, dotLocation);
        this.targetField = target.substring(target.indexOf('.') + 1);
    }

    public void addEntry(ColladaSamplerEntry entry) {
        samplerEntries.add(entry);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("SID", targetSID)
                .add("field", targetField)
                .add("entries", samplerEntries.size())
                .toString();
    }

    public ColladaSamplerEntry getEntryAtTime(float currentTime) {
        ColladaSamplerEntry newEntry;
        ColladaSamplerEntry previous = null;
        ColladaSamplerEntry next = null;
        for (ColladaSamplerEntry samplerEntry : samplerEntries) {
            if (samplerEntry.timeStamp > currentTime) {
                next = samplerEntry;
                break;
            }
            previous = samplerEntry;
        }

        if (next == null && previous == null) {
            //There is no data in the sampler
            return null;
        }
        if (previous == null) {
            //We haven't yet started sampling.
            return null;
        }
        if (next == null) {
            //Animation has finished, previous should have the last value
            //newEntry = previous.duplicate();
            //newEntry.timeStamp = currentTime;
            //1
            // return newEntry;
            return null;
        }

        double previousTimestamp = previous != null ? previous.timeStamp : 0;
        float nextValue = next.value;
        double previousValue = previous != null ? previous.value : nextValue;

        double midwaySection = currentTime - previousTimestamp;
        double percentage = midwaySection / (next.timeStamp - previousTimestamp);

        newEntry = new ColladaSamplerEntry();
        newEntry.timeStamp = currentTime;
        newEntry.value =(float) SNSMaths.interpolate(previousValue, nextValue, percentage);
        newEntry.interpolationType = previous.interpolationType;
        return newEntry;
    }

    public Object getValueAtTime(float currentTime) {
        //currentTime = 0;
        ColladaSamplerEntry previous = null;
        ColladaSamplerEntry next = null;
        for (ColladaSamplerEntry samplerEntry : samplerEntries) {
            if (samplerEntry.timeStamp > currentTime) {
                next = samplerEntry;
                break;
            }
            previous = samplerEntry;
        }

        if (next == null && previous == null) {
            return null;
        }

        if (next == null) {
            //Animation has finished, previous should have the last value
            return previous.value;
        }

        double previousTimestamp = previous != null ? previous.timeStamp : 0;
        float nextValue = next.value;
        double previousValue = previous != null ? previous.value : nextValue;

        double midwaySection = currentTime - previousTimestamp;
        double percentage = midwaySection / (next.timeStamp - previousTimestamp);
        return (float) SNSMaths.interpolate(previousValue, nextValue, percentage);
    }
}
