package mod.steamnsteel.client.collada.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ColladaMeshGeometry extends ColladaGeometry implements Serializable {

    private List<ColladaMeshElement> meshElements = new ArrayList<>();

    public void addMeshElements(Iterable<ColladaMeshElement> colladaMeshElements) {

        for (ColladaMeshElement colladaMeshElement : colladaMeshElements) {
            this.meshElements.add(colladaMeshElement);
        }

    }

    public List<ColladaMeshElement> getMeshElements() {
        return meshElements;
    }
}

