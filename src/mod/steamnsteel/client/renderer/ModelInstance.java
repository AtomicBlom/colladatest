package mod.steamnsteel.client.renderer;

import mod.steamnsteel.client.collada.model.ColladaModel;
import mod.steamnsteel.client.collada.model.ColladaNode;
import mod.steamnsteel.client.collada.model.ColladaScene;
import mod.steamnsteel.client.collada.model.transformation.MatrixTransform;
import mod.steamnsteel.client.collada.model.transformation.TransformationBase;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by Steven on 28/05/2015.
 */
public class ModelInstance {
    private ColladaModel model;
    private MatrixTransform[] meshMatrices;

    public ModelInstance(ColladaModel model) {
        this.model = model;
        meshMatrices = new MatrixTransform[model.getAllNodes().size() + 1];
        buildOptimisedModel(model);
    }

    private void buildOptimisedModel(ColladaModel model) {
        Stack<ColladaNode> nodeStack = new Stack<>();
        for (ColladaScene colladaScene : model.getScenes()) {
            for (ColladaNode colladaNode : colladaScene.getChildren()) {
                processNode(colladaNode);

            }
        }
    }

    private void processNode(ColladaNode colladaNode) {
        for (ColladaNode node : colladaNode.getChildren()) {
            processNode(node);
        }
    }

    public ColladaModel getModel() {
        return model;
    }

    public MatrixTransform getTransformForMesh(int index) {
        return meshMatrices[index];
    }

    public void updateAnimation() {
        //FIXME: Update animation!
        //TODO: Calculate meshMatrices

        Queue<ColladaNode> nodesToProcess = new LinkedList<ColladaNode>(model.getScenes());
        while (!nodesToProcess.isEmpty()) {
            ColladaNode node = nodesToProcess.remove();
            ColladaNode parent = node.getParent();

            if (node.getGeometry() == null && node.getChildren().size() == 0) {
                continue;
            }

            MatrixTransform transform;
            int parentIndex = -1;
            if (parent != null) {
                parentIndex = parent.getIndex();
                transform = meshMatrices[parentIndex].copy();
            }else {
                transform = new MatrixTransform();
            }


            //if (parentIndex == 0) {
                //Process transforms
                for (TransformationBase transformationBase : node.getTransformations()) {
                    transformationBase.applyTo(transform);
                }
            //}

            int nodeIndex = node.getIndex();
            meshMatrices[nodeIndex] = transform;
            nodesToProcess.addAll(node.getChildren());
        }
    }
}
