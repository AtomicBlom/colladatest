package mod.steamnsteel.client.collada.model;

/**
 * Created by Steven on 25/05/2015.
 */
public enum AnimationType {
    ONCE, LOOP
}
