package mod.steamnsteel.client.collada.model;

import com.google.common.base.Objects;
import mod.steamnsteel.client.collada.IPopulatable;
import net.minecraftforge.client.model.obj.TextureCoordinate;
import net.minecraftforge.client.model.obj.Vertex;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.io.Serializable;

/**
 * Created by Steven on 13/04/2015.
 */
public class ColladaVertex implements IPopulatable, Serializable {
    public final Vector3f position;
    public final Vector3f normal;
    public final Vector2f texture;

    public ColladaVertex() {
        position = new Vector3f(0, 0, 0);
        normal = new Vector3f(0, 0, 0);
        texture = new Vector2f(0, 0);
    }

    @Override
    public String toString() {

        return Objects.toStringHelper(this)
                .add("position",
                        Objects.toStringHelper(position)
                                .add("x", position.x)
                                .add("y", position.y)
                                .add("z", position.z)
                )
                .add("normal",
                        Objects.toStringHelper(normal)
                                .add("x", normal.x)
                                .add("y", normal.y)
                                .add("z", normal.z)
                )
                .add("texture",
                        Objects.toStringHelper(texture)
                                .add("u", texture.x)
                                .add("v", texture.y)
                )
                .toString();
    }
}
