package mod.steamnsteel.client.collada.xmltransformer;

/**
 * Created by Steven on 13/04/2015.
 */
public class AnimationDefinitionException extends RuntimeException {
    public AnimationDefinitionException(String s) {
        super(s);
    }

    public AnimationDefinitionException(String s, Throwable t) {
        super(s, t);
    }
}
