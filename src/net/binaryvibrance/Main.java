package net.binaryvibrance;

import de.matthiasmann.twl.utils.PNGDecoder;
import mod.steamnsteel.client.collada.ColladaModelReader;
import mod.steamnsteel.client.collada.model.ColladaModel;
import mod.steamnsteel.client.renderer.ColladaRenderer;
import mod.steamnsteel.client.renderer.ModelInstance;
import mod.steamnsteel.client.renderer.TESRRenderer;
import mod.steamnsteel.utility.log.Logger;
import mod.steamnsteel.utility.math.SNSMaths;
import net.binaryvibrance.gl.GLApp;
import org.lwjgl.opengl.*;
import org.lwjgl.util.glu.*;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Run a bare-bones GLApp.  Draws one white triangle centered on screen.
 * <P>
 * GLApp initializes the LWJGL environment for OpenGL rendering,
 * ie. creates a window, sets the display mode, inits mouse and keyboard,
 * then runs a loop that calls draw().
 * <P>
 * napier at potatoland dot org
 */
public class Main extends GLApp {

    private TESRRenderer renderer;
    private float movementX;
    private float movementZ;

    private float cameraRadius = (float)Math.PI;
    private float cameraDistance = -10;
    private ModelInstance modelInstance;
    private boolean flipModel;

    /**
     * Start the application.  run() calls setup(), handles mouse and keyboard input,
     * calls render() in a loop.
     */
    public static void main(String args[]) {
        // create the app
        Main demo = new Main();

        // set title, window size

        window_title = "Hello World";
        displayWidth = 1280;
        displayHeight = 720;

        // start running: will call init(), setup(), draw(), mouse functions
        demo.run();
    }

    /**
     * Initialize the scene.  Called by GLApp.run().  For now the default
     * settings will be fine, so no code here.
     */
    public void setup() throws IOException {
        ColladaModel model = loadModel();
        loadTextures(model);

        modelInstance = new ModelInstance(model);
        renderer = new TESRRenderer();

        //modelInstance.runAllAnimations();

        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }



    @Override
    public void update() {

        renderer.queueRender(modelInstance);
        cameraRadius += movementX * PIOVER180;
        cameraDistance += movementZ / 2;
    }

    /**
     * Render one frame.  Called by GLApp.run().
     */
    public void draw() {
        // Clear screen and depth buffer
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        // Select The Modelview Matrix (controls model orientation)
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        // Reset the coordinate system to center of screen
        GL11.glLoadIdentity();

        double cameraX = Math.cos(cameraRadius) * cameraDistance;
        double cameraZ = Math.sin(cameraRadius) * cameraDistance;

        // Place the viewpoint
        GLU.gluLookAt(
                (float) cameraX, 0f, (float) cameraZ,   // eye position (10 units in front of the origin)
                0f, 0f, 0f,    // target to look at (the origin)
                0f, 1f, 0f);   // which way is up (Y axis)
        GL11.glPushMatrix();
        if (flipModel) {
            GL11.glScalef(1, -1, 1);
            GL11.glFrontFace(GL11.GL_CW);
        } else {
            GL11.glFrontFace(GL11.GL_CCW);
        }
        renderer.render();
        GL11.glPopMatrix();
    }

    @Override
    public void keyDown(int keycode) {
        super.keyDown(keycode);
        if (keycode == 30) {
            //A
            movementX -= 1;
        }
        if (keycode == 17) {
            //W
            movementZ += 1;
        }
        if (keycode == 31) {
            //S
            movementZ -= 1;
        }
        if (keycode == 32) {
            //D
            movementX += 1;
        }
    }

    @Override
    public void keyUp(int keycode) {
        if (keycode == 30) {
            //A
            movementX += 1;
        }
        if (keycode == 17) {
            //W
            movementZ -= 1;
        }
        if (keycode == 31) {
            //S
            movementZ += 1;
        }
        if (keycode == 32) {
            //D
            movementX -= 1;
        }
        if (keycode == 57) {
            cameraRadius = 0;
            cameraDistance = -10;
        }
        if (keycode == 16) {
            //modelInstance.runAllAnimations();
        }
        if (keycode == 18) {
            flipModel = !flipModel;
        }
    }

    private void loadTextures(ColladaModel model) {
        for (String filename : model.getTextures()) {
            try {
                InputStream in = new FileInputStream(filename);
                PNGDecoder decoder = new PNGDecoder(in);

                int tWidth = decoder.getWidth();
                int tHeight = decoder.getHeight();
                ByteBuffer buf = ByteBuffer.allocateDirect(4 * tWidth * tHeight);
                decoder.decode(buf, tWidth * 4, PNGDecoder.Format.RGBA);
                buf.flip();

                in.close();

                int texId = GL11.glGenTextures();
                GL13.glActiveTexture(GL13.GL_TEXTURE0 );
                GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);

                GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
                GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, tWidth, tHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
                GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

                model.assignTextureId(filename, texId);
            } catch (Exception e) {
                Logger.info("Could not load texture %s, %s", filename, e.toString());
            }
        }
    }

    private ColladaModel loadModel() throws IOException {
        float time      = 0.0f;
        time = SNSMaths.getSystemTime();
        ColladaModel model;
        //model = ColladaModelReader.read(new File("SSSteamSpider.dae"));
        //model = ColladaModelReader.read(new File("SSSteamSpiderPrecompiledMatrices.dae"));
        //model = ColladaModelReader.read(new File("pipewrench.dae"));
        model = ColladaModelReader.read(new File("cube.dae"));

        Logger.info("Loading XML model took %f seconds", (SNSMaths.getSystemTime() - time) / 1000.0f);

        serializeModel(model);

        time = SNSMaths.getSystemTime();
        ColladaModel deserializedModel = deserializeModel("./serialized.snsmodel");
        Logger.info("Loading binary model took %f seconds", (SNSMaths.getSystemTime() - time) / 1000.0f);
        return deserializedModel;
    }

    private void serializeModel(ColladaModel model) {
        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream("./serialized.snsmodel");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(model);
            out.close();
            fileOut.close();
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }

    private ColladaModel deserializeModel(String path) {
        try
        {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ColladaModel e = (ColladaModel) in.readObject();
            in.close();
            fileIn.close();
            return e;
        }catch(IOException i)
        {
            i.printStackTrace();
            return null;
        }catch(ClassNotFoundException c)
        {
            c.printStackTrace();
            return null;
        }
    }

}