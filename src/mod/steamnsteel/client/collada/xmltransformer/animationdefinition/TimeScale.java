package mod.steamnsteel.client.collada.xmltransformer.animationdefinition;

import java.util.HashMap;

/**
 * Created by Steven on 25/05/2015.
 */
public enum TimeScale {
    SECONDS,
    FRAMES;

    private static HashMap<String, TimeScale> values;

    static {
        values = new HashMap<>();
        values.put("seconds", SECONDS);
        values.put("frames", FRAMES);
    }

    public static TimeScale fromString(String value) {
        return values.get(value.toLowerCase());
    }
}
