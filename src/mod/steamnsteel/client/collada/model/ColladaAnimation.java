package mod.steamnsteel.client.collada.model;

import com.google.common.collect.Iterables;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Steven on 7/05/2015.
 */
public class ColladaAnimation implements Serializable {
    private final String name;
    private final AnimationType type;
    public final List<ColladaSampler> samplers = new ArrayList<>();
    private final List<ColladaAnimation> childAnimations = new LinkedList<>();

    public ColladaAnimation(String name) {
        this.name = name;
        this.type = AnimationType.ONCE;
    }

    public ColladaAnimation(String name, AnimationType type) {
        this.name = name;
        this.type = type;
    }

    public void addSampler(ColladaSampler sampler) {
        samplers.add(sampler);
    }

    public void addChildAnimations(Iterable<ColladaAnimation> childAnimations) {
        Iterables.addAll(this.childAnimations, childAnimations);
    }

    public List<ColladaAnimation> getChildAnimations() {
        return childAnimations;
    }

    public String getName() {
        return name;
    }

    public AnimationType getType() {
        return type;
    }
}
