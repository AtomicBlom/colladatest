package mod.steamnsteel.client.collada.xmltransformer.collada;

import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.model.ColladaMaterial;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.util.HashMap;

public class MaterialTransformer extends ColladaTransformerBase<ColladaMaterial> {
    private final Node root;

    public MaterialTransformer(Node root, HashMap<String, Node> identifiedNodes) {
        super(identifiedNodes);
        this.root = root;
    }

    @Nullable
    @Override
    public ColladaMaterial apply(@Nullable Node node) {
        try {
            Node instanceEffectNode = findNode(node, "./instance_effect");
            String url = getAttributeSafe(instanceEffectNode, "url");
            Node effectTechniqueNode = findNode(identifiedNodes.get(url.substring(1)), "./profile_COMMON/technique");
            Node shaderNode = findNode(effectTechniqueNode, "./*[self::blinn or self::lambert or self::phong]");

            ColladaMaterial material = new ColladaMaterial();

            switch (shaderNode.getNodeName()) {
                case "phong":
                    Node textureNode = findNode(shaderNode, "./diffuse/texture");

                    String textureId = getAttributeSafe(textureNode, "texture");
                    Node imageNode = findNode(identifiedNodes.get(textureId), "./init_from");
                    String nodeValue = imageNode.getFirstChild().getNodeValue();

                    material.setDiffuseTexture(new File(nodeValue).getName());
            }
            return material;

        } catch (XPathExpressionException e) {
            throw new ColladaException("Error creating Material", e);
        }
    }
}
