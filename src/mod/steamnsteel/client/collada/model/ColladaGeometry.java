package mod.steamnsteel.client.collada.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Steven on 5/05/2015.
 */
public abstract class ColladaGeometry implements Serializable {

    HashMap<String, ColladaMaterial> mappedMaterials = new HashMap<>();
    private ColladaNode parentNode;

    public void addMaterialMap(String symbol, ColladaMaterial targetMaterial) {
        mappedMaterials.put(symbol, targetMaterial);
    }

    public HashMap<String, ColladaMaterial> getMaterialMap() {
        return mappedMaterials;
    }

    public void setParentNode(ColladaNode parentNode) {
        this.parentNode = parentNode;
    }

    public ColladaNode getParentNode() {
        return parentNode;
    }
}
