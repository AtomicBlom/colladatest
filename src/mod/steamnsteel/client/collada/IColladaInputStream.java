package mod.steamnsteel.client.collada;

/**
 * Created by Steven on 5/05/2015.
 */
public interface IColladaInputStream {
    boolean applyElementsAtIndex(int index, IPopulatable populatable);
}
