package mod.steamnsteel.client.collada.xmltransformer.collada;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.model.*;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import mod.steamnsteel.utility.log.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Nullable;
import javax.xml.xpath.XPathExpressionException;
import java.util.*;

/**
 * Created by Steven on 13/05/2015.
 */
public class ColladaTransformer extends ColladaTransformerBase<ColladaModel> {
    public ColladaTransformer() {
        super(new HashMap<String, Node>());
    }

    @Nullable
    @Override
    public ColladaModel apply(@Nullable Node doc) {
        try {
            ColladaModel result = new ColladaModel();
            Logger.info("\tIdentifying elements");
            NodeList nodes = null;

            nodes = findNodes(doc, "//*[@id]");
            for (Node node : new IterableNodeList(nodes)) {
                identifiedNodes.put(TransformerBase.getAttributeSafe(node, "id"), node);
            }

            Logger.info("\tLoading Geometry");
            NodeList evaluate = findNodes(doc, "/COLLADA/library_visual_scenes/visual_scene");
            Function<Node, ColladaScene> transformVisualScene = new VisualSceneTransformer(doc, identifiedNodes);

            result.addScenes(
                    Iterables.transform(new IterableNodeList(evaluate), transformVisualScene)
            );

            NodeList fpsNode = findNodes(doc, "/COLLADA/library_visual_scenes/visual_scene/extra/technique[@profile='MAX3D']/frame_rate");
            if (fpsNode != null && fpsNode.getLength() > 0) {

                Float fps = Float.parseFloat(fpsNode.item(0).getFirstChild().getNodeValue());
                result.setFps(fps);
            }

            Logger.info("\tPreoptimising nodes");
            optimiseNodes(result);

            Logger.info("\tLoading Animations");
            NodeList animationNodeList = findNodes(doc, "/COLLADA/library_animations/animation");

            Iterable<ColladaAnimation> transform1 = Iterables.transform(new IterableNodeList(animationNodeList), new AnimationTransformer(identifiedNodes, result));
            transform1 = Iterables.filter(transform1, new Predicate<ColladaAnimation>() {
                @Override
                public boolean apply(@Nullable ColladaAnimation colladaAnimation) {
                    return colladaAnimation != null;
                }
            });
            result.addAnimations(transform1);

            HashMap<String, List<ColladaSampler>> allAnimations = new HashMap<>();
            Stack<ColladaAnimation> animationStack = new Stack<>();
            animationStack.addAll(result.getAnimations());

            Logger.info("\tGathering animation samplers");

            //Catalog the samplers
            while (!animationStack.empty()) {
                ColladaAnimation animation = animationStack.pop();
                if (animation == null) continue;
                for (ColladaSampler sampler : animation.samplers) {
                    List<ColladaSampler> samplers = allAnimations.get(sampler.targetSID);
                    if (samplers == null) {
                        samplers = new LinkedList<>();

                        allAnimations.put(sampler.targetSID, samplers);
                    }
                    samplers.add(sampler);
                }
                animationStack.addAll(animation.getChildAnimations());
            }



            Logger.info("\tApplying transformations to samplers");

            HashMap<String, ColladaMaterial> commonMaterials = new HashMap<>();

            for (ColladaScene colladaScene : result.getScenes()) {
                for (ColladaNode colladaNode : colladaScene.getAllNodes()) {
                    ColladaMeshGeometry geometry = colladaNode.getGeometry();
                    if (geometry != null) {
                        HashMap<String, ColladaMaterial> materialMap = geometry.getMaterialMap();

                        Map.Entry<String, ColladaMaterial>[] objects = (Map.Entry<String, ColladaMaterial>[]) materialMap.entrySet().toArray(new Map.Entry[materialMap.size()]);
                        for (Map.Entry<String, ColladaMaterial> object : objects) {
                            String key = object.getKey();
                            if (commonMaterials.containsKey(key)) {
                                materialMap.put(key, commonMaterials.get(key));
                            } else {
                                commonMaterials.put(key, object.getValue());
                            }
                        }
                    }
                }
            }
            result.setMaterials(commonMaterials);
            return result;
        } catch (XPathExpressionException e) {
            throw new ColladaException("Unable to process COLLADA element", e);
        }

    }

    private ArrayList<ColladaNode> optimiseNodes(ColladaModel result) {
        Queue<ColladaNode> nodesToProcess = new LinkedList<ColladaNode>(result.getScenes());
        int arrayIndex = 0;
        ArrayList<ColladaNode> parentMap = new ArrayList<>();
        while (!nodesToProcess.isEmpty()) {
            ColladaNode node = nodesToProcess.remove();
            node.setIndex(arrayIndex);

            ColladaNode parent = node.getParent();
            parentMap.add(arrayIndex, parent);

            arrayIndex++;

            nodesToProcess.addAll(node.getChildren());
        }
        return parentMap;

    }
}
