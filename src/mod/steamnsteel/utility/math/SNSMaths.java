package mod.steamnsteel.utility.math;

import org.lwjgl.Sys;

/**
 * Created by Steven on 11/05/2015.
 */
public class SNSMaths {
    /**
     * @param a The first value
     * @param b The second value
     * @param d The interpolation factor, between 0 and 1
     * @return a+(b-a)*d
     */
    public static double interpolate(double a, double b, double d)
    {
        return a+(b-a)*d;
    }

    public static long getSystemTime()
    {
        return Sys.getTime() * 1000L / Sys.getTimerResolution();
    }
}
