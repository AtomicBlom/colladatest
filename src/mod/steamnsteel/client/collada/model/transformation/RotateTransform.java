package mod.steamnsteel.client.collada.model.transformation;

import com.google.common.base.Objects;
import mod.steamnsteel.client.collada.model.ColladaSampler;
import org.lwjgl.opengl.GL11;

import java.io.Serializable;

/**
 * Created by Steven on 8/05/2015.
 */
public class RotateTransform extends TransformationBase<Float> implements Serializable {
    private float angle;
    private float x;
    private float y;
    private float z;

    public RotateTransform(float angle, float x, float y, float z) {

        this.angle = angle;
        this.x = x;
        this.y = y;
        this.z = z;
    }


    @Override
    public void applyTo(MatrixTransform nodeMatrix) {
        nodeMatrix.rotate(Math.toRadians(angle), x, y, z);
        //nodeMatrix.rotate(angle, x, y, z);
    }

    @Override
    protected Object getValueInternal(ColladaSampler sampler) {
        switch (sampler.targetField) {
            case "ANGLE":
                return angle;
        }
        return null;
    }

    @Override
    protected void setValueInternal(ColladaSampler sampler, Float o) {
        switch (sampler.targetField) {
            case "ANGLE":
                angle = o;
                break;
        }
    }

    @Override
    public TransformationBase clone() {
        return new RotateTransform(angle, x, y, z);
    }

    @Override
    public void applyToGL() {
        GL11.glRotated(angle, x, y, z);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("angle", String.format("%f", angle))
                .add("x", x)
                .add("y", y)
                .add("z", z)
                .toString();
    }
}
