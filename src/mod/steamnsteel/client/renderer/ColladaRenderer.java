package mod.steamnsteel.client.renderer;

import mod.steamnsteel.client.collada.model.*;
import mod.steamnsteel.client.collada.model.transformation.TransformationBase;
import net.minecraftforge.client.model.obj.Vertex;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import java.util.HashMap;

/**
 * Created by codew on 1/05/2015.
 */
public class ColladaRenderer {

    private final ModelInstance model;
    private static final Vertex WORKING_VERTEX = new Vertex(0, 0, 0);
    private int currentDrawMode;

    public ColladaRenderer(ModelInstance model) {
        this.model = model;
    }

    private MeshGeometryType currentType = null;
    private ColladaMaterial currentMaterial = null;

    public void render() {
        //model.updateRunningAnimations();
        currentType = null;
        currentMaterial = null;

        for (ColladaMaterial colladaMaterial : model.getModel().getMaterials()) {
            currentMaterial = colladaMaterial;
            bindMaterialForRender(currentMaterial);
            for (ColladaScene scene : model.getModel().getScenes()) {
                for (ColladaNode node : scene.getChildren()) {
                    renderNode(node);
                }
            }

            /*if (currentDrawMode != Integer.MIN_VALUE) {
                GL11.glEnd();
            }
            GL11.glPopMatrix();*/
        }
    }

    private void renderNode(ColladaNode node) {
        GL11.glPushMatrix();
        for (TransformationBase transform : node.getTransformations()) {
            if (node.getSid() == null || transform.getSid() == null) {
                transform.applyToGL();
            } else {
                String s = node.getSid() + "/" + transform.getSid();
                /*Iterable<ColladaRunningAnimation> runningAnimationsForTransform = model.getRunningAnimationsForTransform(s);
                if (runningAnimationsForTransform != null) {
                    for (ColladaRunningAnimation colladaRunningAnimation : runningAnimationsForTransform) {
                        transform = colladaRunningAnimation.getAnimatedTransform(transform);
                    }
                }*/

                transform.applyToGL();
            }
        }

        ColladaMeshGeometry geometry = node.getGeometry();
        if (geometry != null) {
            HashMap<String, ColladaMaterial> materialMap = geometry.getMaterialMap();
            for (ColladaMeshElement colladaMeshElement : geometry.getMeshElements()) {
                if (colladaMeshElement instanceof ColladaMeshElementComposite) {
                    renderCompositeMesh((ColladaMeshElementComposite) colladaMeshElement, materialMap);
                } else {
                    renderMesh((ColladaMeshElementSimple) colladaMeshElement, materialMap);
                }
            }
        }

        for (ColladaNode colladaNode : node.getChildren()) {
            renderNode(colladaNode);
        }
        GL11.glPopMatrix();
    }

    private void renderCompositeMesh(ColladaMeshElementComposite colladaMeshElement, HashMap<String, ColladaMaterial> materialMap) {
        for (ColladaMeshElementSimple colladaSimpleMeshElement : colladaMeshElement.getChildElements()) {
            renderMesh(colladaSimpleMeshElement, materialMap);
        }
    }

    private void renderMesh(ColladaMeshElementSimple colladaMeshElement, HashMap<String, ColladaMaterial> materialMap) {
        String material = colladaMeshElement.getMaterialKey();

        if (currentMaterial == null || currentMaterial.getTextureId() != materialMap.get(material).getTextureId()) {
            return;
        }

        currentDrawMode = colladaMeshElement.getType().drawMode;
        GL11.glBegin(currentDrawMode);

        for (ColladaVertex vertex : colladaMeshElement.getVertices()) {
            WORKING_VERTEX.x = vertex.position.x;
            WORKING_VERTEX.y = vertex.position.y;
            WORKING_VERTEX.z = vertex.position.z;

            GL11.glNormal3f(vertex.normal.x, vertex.normal.y, vertex.normal.z);
            GL11.glTexCoord2f(vertex.texture.x, vertex.texture.y);
            GL11.glVertex3f(WORKING_VERTEX.x, WORKING_VERTEX.y, WORKING_VERTEX.z);
        }

        GL11.glEnd();
    }

    private void bindMaterialForRender(ColladaMaterial material) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        int textureId = material.getTextureId();
        //if (textureId == 2) {
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
        //}
    }

    /*

    public void tesselate() {

        model.updateRunningAnimations();
        currentType = null;
        currentMaterial = null;
        for (ColladaScene scene : model.getModel().getScenes()) {
            for (ColladaNode node : scene.getNodes()) {
                tesselateNode(node, MatrixTransform.getIdentity());
            }
        }
        Tessellator.instance.draw();
    }


    private void tesselateNode(ColladaNode node, MatrixTransform identity) {
        MatrixTransform nodeMatrix = MatrixTransform.getIdentity();

        for (TransformationBase transform : node.getTransformations()) {
            String s = node.getSid() + "/" + transform.getSid();
            Iterable<ColladaRunningAnimation> runningAnimationsForTransform = model.getRunningAnimationsForTransform(s);
            if (runningAnimationsForTransform != null) {
                for (ColladaRunningAnimation colladaRunningAnimation : runningAnimationsForTransform) {
                    transform = colladaRunningAnimation.getAnimatedTransform(transform);
                }
            }

            transform.applyTo(nodeMatrix);
        }
        nodeMatrix.leftMultiply(identity);

        ColladaMeshGeometry geometry = node.getGeometry();
        if (geometry != null) {
            HashMap<String, ColladaMaterial> materialMap = geometry.getMaterialMap();
            for (ColladaMeshElement colladaMeshElement : geometry.getMeshElements()) {
                if (colladaMeshElement instanceof ColladaMeshElementComposite) {
                    tesselateCompositeMesh((ColladaMeshElementComposite) colladaMeshElement, nodeMatrix, materialMap);
                } else {
                    tesselateMesh((ColladaMeshElementSimple) colladaMeshElement, nodeMatrix, materialMap);
                }
            }
        }

        for (ColladaNode colladaNode : node.getChildren()) {
            renderNode(colladaNode, nodeMatrix);
        }
    }

    private void tesselateCompositeMesh(ColladaMeshElementComposite colladaMeshElement, MatrixTransform matrix, HashMap<String, ColladaMaterial> materialMap) {
        for (ColladaMeshElementSimple colladaSimpleMeshElement : colladaMeshElement.getChildElements()) {
            tesselateMesh(colladaSimpleMeshElement, matrix, materialMap);
        }
    }

    private void tesselateMesh(ColladaMeshElementSimple colladaMeshElement, MatrixTransform matrix, HashMap<String, ColladaMaterial> materialMap) {
        startDrawing(colladaMeshElement.getType());
        String material = colladaMeshElement.getMaterialKey();
        if (currentMaterial == null || !currentMaterial.equals(material)) {
            ColladaMaterial material1 = materialMap.get(material);
            bindMaterial(material1);
            currentMaterial = material;
        }

        for (ColladaVertex vertex : colladaMeshElement.getVertices()) {
            WORKING_VERTEX.x = vertex.position.x;
            WORKING_VERTEX.y = vertex.position.y;
            WORKING_VERTEX.z = vertex.position.z;

            matrix.apply(WORKING_VERTEX);

            Tessellator.instance.setNormal(vertex.normal.x, vertex.normal.y, vertex.normal.z);
            Tessellator.instance.setTextureUV(vertex.texture.u, vertex.texture.v);
            Tessellator.instance.addVertex(WORKING_VERTEX.x, WORKING_VERTEX.y, WORKING_VERTEX.z);
        }
    }

    private void bindMaterial(ColladaMaterial material) {
        String texture = material.getDiffuseTexture();

        if (false) {
            ResourceLocation locationSkin = Minecraft.getMinecraft().thePlayer.getLocationSkin();
            Minecraft.getMinecraft().getTextureManager().bindTexture(locationSkin);
        } else {
            ResourceLocation resourceLocation = new ResourceLocation(TheMod.MOD_ID.toLowerCase(), "textures/models/" + texture);
            Minecraft.getMinecraft().getTextureManager().bindTexture(resourceLocation);
        }
    }

    private void startTesselating(MeshGeometryType type) {
        if (currentType != type) {
            if (currentType != null) {
                Tessellator.instance.draw();
            }

            if (type == MeshGeometryType.QUADS) {
                Tessellator.instance.startDrawing(GL11.GL_QUADS);
            } else if (type == MeshGeometryType.TRIANGLES) {
                Tessellator.instance.startDrawing(GL11.GL_TRIANGLES);
            }
            currentType = type;
        }
    }*/
}
