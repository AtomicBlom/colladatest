package mod.steamnsteel.client.collada.model;

import com.google.common.collect.Iterables;
import mod.steamnsteel.client.collada.model.transformation.TransformationBase;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Steven on 30/04/2015.
 */
public class ColladaNode implements Serializable {
    private final String name;
    private final String sid;
    private ColladaMeshGeometry geometry;

    private final LinkedList<TransformationBase> transformations = new LinkedList<>();
    LinkedList<ColladaNode> children = new LinkedList<>();
    LinkedList<ColladaNode> allChildren = new LinkedList<>();
    private ColladaNode parent;
    private int index;

    public ColladaNode(String name, String sid) {
        this.name = name;
        this.sid = sid;
    }


    private List<String> layers;

    public void addNodes(Iterable<ColladaNode> children) {
        for (ColladaNode child : children) {
            allChildren.addAll(child.allChildren);
            allChildren.add(child);
            this.children.add(child);
            child.addParent(this);
        }
    }

    private void addParent(ColladaNode parent) {

        this.parent = parent;
    }

    public void setLayers(List<String> layers) {
        this.layers = layers;
    }

    public List<String> getLayers() {
        return layers;
    }

    public void setGeometry(ColladaMeshGeometry geometry) {
        this.geometry = geometry;
        geometry.setParentNode(this);
    }

    public ColladaMeshGeometry getGeometry() {
        return geometry;
    }

    public LinkedList<ColladaNode> getChildren() {
        return children;
    }

    public void addTransformations(Iterable<TransformationBase> transformations) {
        Iterables.addAll(this.transformations, transformations);
    }

    public Iterable<TransformationBase> getTransformations() {
        return transformations;
    }

    public String getSid() {
        return sid;
    }

    public ColladaNode getParent() {
        return parent;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
