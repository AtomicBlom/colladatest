package mod.steamnsteel.client.collada.model;

import org.lwjgl.opengl.GL11;

/**
 * Created by Steven on 24/04/2015.
 */
public enum MeshGeometryType {
    TRIANGLES(GL11.GL_TRIANGLES),
    QUADS(GL11.GL_QUADS),
    POLY_LIST(GL11.GL_POLYGON),
    ;
    public final int drawMode;

    MeshGeometryType(int drawMode) {

        this.drawMode = drawMode;
    }
}
