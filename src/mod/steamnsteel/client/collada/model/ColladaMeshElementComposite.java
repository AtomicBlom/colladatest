package mod.steamnsteel.client.collada.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Steven on 27/04/2015.
 */
public class ColladaMeshElementComposite extends ColladaMeshElement implements Serializable {

    public ColladaMeshElementComposite(ColladaNode parentNode) {
        super(parentNode);
    }

    private List<ColladaMeshElementSimple> simpleMeshElements = new ArrayList<>();

    public void addChildElements(Collection<ColladaMeshElementSimple> simpleMeshElements) {

        this.simpleMeshElements.addAll(simpleMeshElements);
    }

    public List<ColladaMeshElementSimple> getChildElements() {
        return simpleMeshElements;
    }
}
