package mod.steamnsteel.client.collada.model;

import java.io.Serializable;

public abstract class ColladaMeshElement implements Serializable {
    private String materialKey;
    private final ColladaNode parentNode;

    protected ColladaMeshElement(ColladaNode parentNode) {
        this.parentNode = parentNode;
    }

    public void setMaterialKey(String materialKey) {
        this.materialKey = materialKey;
    }

    public String getMaterialKey() {
        return materialKey;
    }

    public ColladaNode getParentNode() {
        return parentNode;
    }
}

