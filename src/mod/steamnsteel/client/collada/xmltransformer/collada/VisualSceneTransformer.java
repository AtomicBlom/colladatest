package mod.steamnsteel.client.collada.xmltransformer.collada;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.IterableNodeList;
import mod.steamnsteel.client.collada.model.ColladaNode;
import mod.steamnsteel.client.collada.model.ColladaScene;
import mod.steamnsteel.client.collada.xmltransformer.TransformerBase;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Nullable;
import javax.xml.xpath.XPathExpressionException;
import java.util.HashMap;

public class VisualSceneTransformer extends ColladaTransformerBase<ColladaScene> {

    private final Node root;

    public VisualSceneTransformer(Node root, HashMap<String, Node> identifiedNodes) {
        super(identifiedNodes);
        this.root = root;
    }

    @Nullable
    @Override
    public ColladaScene apply(Node xmlVisualScene) {
        try {
            String name = getAttributeSafe(xmlVisualScene, "name");
            ColladaScene newScene = new ColladaScene(name);

            Function<Node, ColladaNode> transformNode = new NodeTransformer(root, identifiedNodes);

            NodeList nodeList = findNodes(xmlVisualScene, "./node");

            newScene.addNodes(
                    Iterables.transform(new IterableNodeList(nodeList), transformNode)
            );

            return newScene;
        } catch (XPathExpressionException e) {
            throw new ColladaException("Error creating Visual Scene", e);
        }
    }
}