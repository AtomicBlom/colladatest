package mod.steamnsteel.client.collada.model;

import com.google.common.base.Objects;
import mod.steamnsteel.client.collada.IPopulatable;

import java.io.Serializable;

/**
 * Created by Steven on 25/05/2015.
 */
public class ColladaSamplerEntry implements IPopulatable, Serializable {
    public float timeStamp;
    public float value;
    public String interpolationType;

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("timeStamp",timeStamp)
                .add("value", value)
                .add("interpolation", interpolationType)
                .toString();
    }

    public ColladaSamplerEntry duplicate() {
        ColladaSamplerEntry entry = new ColladaSamplerEntry();
        entry.timeStamp = timeStamp;
        entry.value = value;
        entry.interpolationType = interpolationType;
        return entry;

    }
}
