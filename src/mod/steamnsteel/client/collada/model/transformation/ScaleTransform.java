package mod.steamnsteel.client.collada.model.transformation;

import com.google.common.base.Objects;
import mod.steamnsteel.client.collada.model.ColladaSampler;
import org.lwjgl.opengl.GL11;

import java.io.Serializable;

/**
 * Created by Steven on 8/05/2015.
 */
public class ScaleTransform extends TransformationBase<Float> implements Serializable {
    private float x;
    private float y;
    private float z;

    public ScaleTransform(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void applyTo(MatrixTransform nodeMatrix) {
        nodeMatrix.scale(x, y, z);
    }

    @Override
    protected Object getValueInternal(ColladaSampler sampler) {
        switch (sampler.targetField) {
            case "X":
                return x;
            case "Y":
                return y;
            case "Z":
                return z;
        }
        return null;
    }

    @Override
    protected void setValueInternal(ColladaSampler sampler, Float o) {
        switch (sampler.targetField) {
            case "X":
                x = o;
                break;
            case "Y":
                y = o;
                break;
            case "Z":
                z = o;
                break;
        }
    }

    @Override
    public TransformationBase clone() {
        return new ScaleTransform(x, y, z);
    }

    @Override
    public void applyToGL() {
        GL11.glScaled(x, y, z);
    }


    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("x", x)
                .add("y", y)
                .add("z", z)
                .toString();
    }
}
