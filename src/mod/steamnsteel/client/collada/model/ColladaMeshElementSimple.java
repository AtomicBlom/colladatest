package mod.steamnsteel.client.collada.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ColladaMeshElementSimple extends ColladaMeshElement implements Serializable {
    private final MeshGeometryType type;
    private final List<ColladaVertex> vertices = new LinkedList<>();

    public ColladaMeshElementSimple(ColladaNode parentNode, MeshGeometryType type) {
        super(parentNode);
        this.type = type;
    }

    public void addVertex(ColladaVertex vertex) {
        vertices.add(vertex);
    }

    public List<ColladaVertex> getVertices() {
        return vertices;
    }

    public MeshGeometryType getType() {
        return type;
    }
}
