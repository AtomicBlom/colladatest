package mod.steamnsteel.client.collada;

import mod.steamnsteel.client.collada.model.*;
import mod.steamnsteel.client.collada.xmltransformer.ColladaException;
import mod.steamnsteel.client.collada.xmltransformer.animationdefinition.AnimationDefinitionTransformer;
import mod.steamnsteel.client.collada.xmltransformer.collada.ColladaTransformer;
import mod.steamnsteel.utility.log.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by codew on 2/05/2015.
 */
public class ColladaModelReader {

    private Document doc;
    private Document animationDefinitionDoc;

    private ColladaModelReader() {
    }

    /*public static ColladaModel read(ResourceLocation resourceLocation) throws IOException, ColladaException {
        IResource res = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
        return read(res.getInputStream());
    }*/

    public static ColladaModel read(File file) throws IOException, ColladaException {
        String filename = file.getName();
        Logger.info("Loading %s", filename);
        File animationDefinitions = new File(file.getParent(), filename.substring(0, filename.indexOf('.')) + ".anim.xml");
        if (animationDefinitions.exists()) {
            Logger.info("Loading animations from %s", animationDefinitions.getName());
            return read(new FileInputStream(file), new FileInputStream(animationDefinitions));
        } else {
            Logger.info("No animation definitions could be found.");
            return read(new FileInputStream(file), null);
        }
    }

    public static ColladaModel read(InputStream file, InputStream animationDefinition) throws IOException, ColladaException {

        ColladaModelReader modelReader = new ColladaModelReader();
        try {
            return modelReader.readInternal(file, animationDefinition);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ColladaException("Error Reading Collada File", e);
        }
    }

    private ColladaModel readInternal(InputStream file, InputStream animationDefinition) throws IOException, ColladaException, ParserConfigurationException, SAXException, XPathExpressionException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        this.doc = builder.parse(file);

        ColladaModel colladaModel = parseColladaXml();
        if (animationDefinition != null) {
            //factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            this.animationDefinitionDoc = builder.parse(animationDefinition);

            applyAnimationDefinition(colladaModel);
        }
        return colladaModel;
    }

    private void applyAnimationDefinition(ColladaModel colladaModel) {
        AnimationDefinitionTransformer transformer = new AnimationDefinitionTransformer(colladaModel);
        transformer.apply(animationDefinitionDoc);
    }

    private ColladaModel parseColladaXml() throws ColladaException, XPathExpressionException {
        ColladaTransformer transformer = new ColladaTransformer();
        ColladaModel model = transformer.apply(doc);

        return model;
    }
}
