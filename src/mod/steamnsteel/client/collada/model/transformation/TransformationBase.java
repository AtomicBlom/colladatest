package mod.steamnsteel.client.collada.model.transformation;

import mod.steamnsteel.client.collada.model.ColladaSampler;

import java.io.Serializable;

/**
 * Created by Steven on 8/05/2015.
 */
public abstract class TransformationBase<T> implements Serializable  {
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    String sid;

    abstract public void applyTo(MatrixTransform nodeMatrix);

    public final void setValue(ColladaSampler sampler, Object o) {
        this.setValueInternal(sampler, (T)o);
    }

    public final Object getValue(ColladaSampler sampler) {
        return this.getValueInternal(sampler);
    }

    protected abstract Object getValueInternal(ColladaSampler sampler);

    protected abstract void setValueInternal(ColladaSampler sampler, T o);

    public abstract TransformationBase clone();

    public abstract void applyToGL();
}
