package mod.steamnsteel.client.collada.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by codew on 1/05/2015.
 */
public class ColladaScene extends ColladaNode implements Serializable {

    public ColladaScene(String name) {
        super(name, null);
    }

    public Iterable<? extends ColladaNode> getAllNodes() {
        return this.allChildren;
    }
}
