package mod.steamnsteel.client.collada.model;

import com.google.common.collect.Iterables;

import java.io.Serializable;
import java.util.*;

public class ColladaModel implements Serializable {
    private final List<ColladaScene> scenes = new ArrayList<>();
    private final UUID id;
    private HashMap<String, ColladaMaterial> materials;
    private final Map<Integer, List<ColladaMeshElementSimple>> geometryPerTextureId = new HashMap<>();
    private Float fps;

    public ColladaModel() {
        this.id = UUID.randomUUID();
    }

    public List<ColladaAnimation> getAnimations() {
        return animations;
    }

    private final List<ColladaAnimation> animations = new ArrayList<>();

    public void addScenes(Iterable<ColladaScene> scenes) {
        Iterables.addAll(this.scenes, scenes);
    }

    public List<ColladaScene> getScenes() {
        return scenes;
    }

    public Collection<ColladaNode> getAllNodes() {
        LinkedList<ColladaNode> iterable = new LinkedList<>();
        for (ColladaScene scene : scenes) {
            Iterables.addAll(iterable, scene.getAllNodes());
        }
        return iterable;
    }

    public void addAnimations(Iterable<ColladaAnimation> animations) {
        Iterables.addAll(this.animations, animations);
    }

    public void setMaterials(HashMap<String, ColladaMaterial> materials) {
        this.materials = materials;
    }

    public Collection<ColladaMaterial> getMaterials() {
        return this.materials.values();
    }

    public Iterable<String> getTextures() {
        List<String> filenames = new LinkedList<>();
        for (ColladaMaterial colladaMaterial : materials.values()) {
            String diffuseTexture = colladaMaterial.getDiffuseTexture();
            filenames.add(diffuseTexture);
        }
        return filenames;
    }

    public void assignTextureId(String filename, int texId) {
        for (ColladaMaterial colladaMaterial : materials.values()) {
            String diffuseTexture = colladaMaterial.getDiffuseTexture();
            if (diffuseTexture.equals(filename)) {
                colladaMaterial.setTextureId(texId);

                List<ColladaMeshElementSimple> meshesByTextureId = geometryPerTextureId.get(texId);
                if (meshesByTextureId == null) {
                    meshesByTextureId = new LinkedList<>();
                    geometryPerTextureId.put(texId, meshesByTextureId);
                }

                //FIXME: assign meshes to textureIds
                for (ColladaNode colladaNode : this.getAllNodes()) {
                    ColladaMeshGeometry geometry = colladaNode.getGeometry();

                    if (geometry != null) {
                        for (Map.Entry<String, ColladaMaterial> stringColladaMaterialEntry : geometry.getMaterialMap().entrySet()) {
                            if (stringColladaMaterialEntry.getValue().getDiffuseTexture().equals(filename)) {
                                for (ColladaMeshElement colladaMeshElement : geometry.getMeshElements()) {
                                    if (colladaMeshElement instanceof ColladaMeshElementComposite) {
                                        for (ColladaMeshElementSimple colladaMeshElementSimple : ((ColladaMeshElementComposite) colladaMeshElement).getChildElements()) {
                                            meshesByTextureId.add(colladaMeshElementSimple);
                                        }
                                    } else {
                                        meshesByTextureId.add((ColladaMeshElementSimple)colladaMeshElement);
                                    }
                                }

                            }
                        }
                    }

                }
            }
        }
    }

    public Float getFps() {
        return fps;
    }

    public void setFps(Float fps) {
        this.fps = fps;
    }

    public UUID getId() {
        return id;
    }

    public List<ColladaMeshElementSimple> getMeshesByTextureId(int textureId) {
        return geometryPerTextureId.get(textureId);
    }
}


